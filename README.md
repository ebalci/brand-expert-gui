# Brand Expert Gui    &nbsp; &nbsp; &nbsp; ![](https://gitlab.cern.ch/ebalci/brand-expert-gui/badges/master/coverage.svg)  ![](https://gitlab.cern.ch/ebalci/brand-expert-gui/badges/master/pipeline.svg)

Expert GUI for BRAND FESA Class

System Portal - BRAND: https://wikis.cern.ch/display/BEBI/System+portal+%3A+BRAND


FESA - BRAND: https://wikis.cern.ch/display/BEBI/Software+-+FESA+%3A+LHC+%3A+BRAND+-+Luminosity+Cherenkov


### Launch

You can launch this application in several ways.

#### [BI Python GUI Manager](https://gitlab.cern.ch/bisw-python/bipy-gui-manager)

If the app has been deployed to the operational folder, you can launch it with:

```bash
/user/bdisoft/operational/python/gui/bipy-gui-manager run -o brand-expert-gui
```

If the app has been deployed to the development folder, you can launch it with:

```bash
/user/bdisoft/operational/python/gui/bipy-gui-manager run -d brand-expert-gui
```

#### [BI Launcher](https://gitlab.cern.ch/bisw-java-fwk/bi-launcher)

If it has been registered in the BI Launcher, you can launch it in this way:

```bash
/user/bdisoft/operational/tools/bi-launcher/launcher <app name in the Launcher>
```

Ask the developers for the application name if you don't know it,
or execute the command above without the application name and look for it in the
window that opens.

## Usage

_Detail how to use the application, how does it look like, where to find more documentation, etc..._


## Contribute -  For Developers
#### Install
To get a ready-to-edit installation, do the following:

- Clone the repo:
```bash
git clone https://:@gitlab.cern.ch:8443/ebalci/brand-expert-gui.git
```

- Setup the virtual environment:

```bash
cd brand-expert-gui/
source /acc/local/share/python/acc-py/base/2020.11/setup.sh
acc-py venv venv
source venv/bin/activate
```

- Do a full install of the app

```bash
pip install -e .[all]
```

#### Setup

Before starting to work, type:

```shell
cd brand-expert-gui/
git pull
source activate.sh
```

`source activate.sh` will activate the virtualenvs in the right order, and alias `bipy-gui-manager` and `pycharm`.

#### Tests
You can run tests with:

```shell
python -m pytest --random-order
```

Note that the tests will automatically regenerate the views before running.

To see the coverage report, type:

```shell
python -m pytest --cov=brand_expert_gui
```

If the tests hang, probably Qt is swallowing errors without exiting. Note that
this can happen for the same reasons on GitLab CI. To see the stacktrace,
re-run the tests as:

```shell
python -m pytest --vv --log-cli-level=DEBUG
```

## Support

To get help, please contact Elif Balci at this email address: elif.balci@cern.ch
