from typing import cast

from PyQt5.QtWidgets import QTableWidgetItem, QHeaderView

colors = ['#4f7d96', '#3cb371', '#990000', '#5b3f5b', 'w']


def setup_table(table, data):
    table.setColumnCount(0)
    table.setRowCount(0)

    if len(data) == 0:
        return

    for i in range(len(data[0])):
        table.insertColumn(i)

    for y, data_line in enumerate(data):
        table.insertRow(y)
        for x, d in enumerate(data_line):
            table.setItem(y, x, QTableWidgetItem(str(d)))

    table.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)
    table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)


def update_table(table, data):
    for y, data_line in enumerate(data):
        for x, d in enumerate(data_line):
            table.setItem(y, x, QTableWidgetItem(str(d)))
    table.resizeColumnsToContents()


def get_table_data(table, types, dropdowns=None):
    print("dr", dropdowns)
    model = table.model()
    data = []
    dropdown_index = 0
    for row in range(model.rowCount()):
        data.append([])
        for column in range(model.columnCount()):
            index = model.index(row, column)
            # We suppose data are strings
            if column is not 0:
                if types[row] == bool:
                    if model.data(index) in ["True", "true"]:
                        data[row].append(True)
                    else:
                        data[row].append(False)
                else:
                    if type(model.data(index)) == str:
                        if dropdowns is not None and dropdowns is not [] and any(c.isalpha() for c in model.data(index)):
                            print(dropdowns[dropdown_index].currentText(), model.data(index))
                            data[row].append(dropdowns[dropdown_index].currentText())
                            dropdown_index = dropdown_index + 1
                        else:
                            data[row].append(types[row](model.data(index)))
                    else:
                        data[row].append(types[row](model.data(row, column)))
            else:
                data[row].append(str(model.data(index)))
    print(data)
    return array_to_dict(data)


def array_to_dict(array):
    dict = {}
    for row in array:
        key = ''
        data = []
        for i in range(len(row)):
            if i == 0:
                key = row[i]
            elif len(row) == 2:
                dict[key] = row[i]
            else:
                data.append(row[i])
        if len(data) != 0:
            dict[key] = data

    return dict
