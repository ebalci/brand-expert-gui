"""
For reference, see:
https://acc-py.web.cern.ch/gitlab/bisw-python/pyqt-tutorial/docs/stable/2-project-structure.html#project-name-constants-py
"""

APPLICATION_NAME = "Brand Expert Gui"
AUTHOR = "Elif Balci"
EMAIL = "elif.balci@cern.ch"
DEVICE_LIST = ["BRAND.device2", "BRAND.device3", "BRAND.device4", "LHC.BRAND.1R", "LHC.BRAND.1L", "LHC.BRAND.5L", "LHC.BRAND.5R"]
SOLENOID_DICT = {"BRAND.device2": "BRANDTEST.SOL", "LHC.BRAND.1R": "LHC.BRAN.SOL.1R", "LHC.BRAND.1L": "LHC.BRAN.SOL.1L",
                 "LHC.BRAND.5L": "LHC.BRAN.SOL.5L", "LHC.BRAND.5R": "LHC.BRAN.SOL.5R",
                 "BRAND.device3": "BRANDTEST.SOL"}
SOLENOID_B_DICT = {"BRAND.device2": "BRANDTEST.SOLB", "LHC.BRAND.1R": "LHC.BRAN.SOLB.1R",
                   "LHC.BRAND.1L": "LHC.BRAN.SOLB.1L",
                   "LHC.BRAND.5L": "LHC.BRAN.SOLB.5L", "LHC.BRAND.5R": "LHC.BRAN.SOLB.5R",
                   "BRAND.device3": "BRANDTEST.SOLB"}
PLOT_LIST = ["ExpertAcquisition#rawData", "ExpertAcquisition#luminosityPerChannel",
             "LuminosityPerBunch#luminosityPerBunch"]
TOTAL_LUMINOSITY = "LuminosityBunchSum#totalLuminosityBunchSum"

BUNCH_NUMBER = 3564
CHANNEL_NUMBER = 8
ADC_NUMBER = 2
SUBSCRIBE = "SUBSCRIBE"
UNSUBSCRIBE = "UNSUBSCRIBE"
VERSION = "0.1.0"
MIN_BRAND_VERSION = "1.10.0"
CALIBRATION_SETTING_HEADERS = ['Field', 'INTEGRATING', 'OCCURRENCE', 'SUMMING']
AMPLIFIER_OFFSET_HEADERS  = ['AmplifierOffset', 'Ch0', 'Ch1', 'Ch2', 'Ch3', 'Ch4', 'Ch5', 'Ch6', 'Ch7']
AMPLIFIER_GAIN_HEADERS = ['AmplifierGain', "Value"]
EXPERT_SETTINGS_SINGLE_HEADERS = ['Field', 'Value']
EXPERT_SETTINGS_DOUBLE_HEADERS = ['Field', 'ADC-0', "ADC-1"]
EXPERT_SETTINGS_MULTIPLE_HEADERS = ['Field', 'Ch0', 'Ch1', 'Ch2', 'Ch3', 'Ch4', 'Ch5', 'Ch6', 'Ch7']
WORKING_MODE_ENUM = {"INTEGRATING": 0, "OCCURRENCE": 1, "SUMMING": 2}
EMULATOR_TIMING_ENUM = {"EMULATOR_CPS": 0, "EMULATOR_SPS": 1, "EMULATOR_LHC": 2, "EMULATOR_UNDEFINED": 3}
TIMING_ENUM = {"BST_SOURCE_OFF": 0, "BST_SOURCE_OPTICS": 1, "BST_SOURCE_EMULATOR": 2, "BST_SOURCE_P0": 3}
INSTRUMENT_MODE_ENUM = {"CHECK": 0, "NORMAL": 1}
COLORS = ["#FF0000", "#FFFF00", "#F38D11", "#00FF00", "#00FFFF", "#FF00FF", "#0000FF", "#C0C0C0"]
