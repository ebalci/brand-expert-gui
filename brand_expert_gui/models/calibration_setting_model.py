from PyQt5.QtCore import pyqtSignal, QObject
from numpy import array, double

from brand_expert_gui.constants import CHANNEL_NUMBER


class CalibrationSettingModel(QObject):
    on_settings_received_signal = pyqtSignal(bool)
    on_amplifier_settings_received_signal = pyqtSignal(bool)

    def __init__(self, device, japc):
        super().__init__()
        self.device = device
        self.japc = japc

        self.raw_data_selection = 0
        self.luminosity_channel_selection = 0
        self.field = "CalibrationSetting"
        self.fields_new = ["constant", "offset"]
        self.fields_old = ["bunchSumConstant", "bunchSumOffset", "perBunchConstant", "perBunchOffset"]
        self.fields = self.fields_new
        self.amplifier_property = "AmplifierSettings"
        self.amplifier_fields = ["amplifierGain", "amplifierOffset"]
        self.amplifier_gain = 0
        self.amplifier_offset = [0] * CHANNEL_NUMBER
        self.types = [double, double]
        self.workingOrder = None
        self.result = []
        self.get_calibration()

    def get_calibration(self):
        try:
            params = self.japc.getParam("{}/{}".format(self.device, self.field))
            self.result = []
            for i in self.fields:
                temp = [i]
                for par in params[i]:
                    temp.append(par)
                self.result.append(temp)
            self.workingOrder = params['workingModeOrder']
            self.on_settings_received_signal.emit(True)
        except Exception as e:
            print(e)
            if self.fields == self.fields_new:
                self.fields = self.fields_old
            else:
                self.fields = self.fields_new
            self.fields = ["bunchSumConstant", "bunchSumOffset", "perBunchConstant", "perBunchOffset"]

    def get_amplifier(self):
        try:
            self.amplifier_gain = self.japc.getParam("{}/{}#{}".format(self.device, self.amplifier_property, self.amplifier_fields[0]))
            self.amplifier_offset = self.japc.getParam("{}/{}#{}".format(self.device, self.amplifier_property, self.amplifier_fields[1]))

            self.on_amplifier_settings_received_signal.emit(True)
        except Exception as e:
            print(e)
            self.amplifier_gain = None
            self.amplifier_fields = [None] * CHANNEL_NUMBER

    def set_device(self, device):
        self.device = device

    def set(self, data):
        self.japc.setParam("{}/{}".format(self.device, self.field), data, checkDims=False)

    def set_amplifier(self, data):
        self.japc.setParam("{}/{}".format(self.device, self.amplifier_property), data, checkDims=False)