from PyQt5.QtCore import pyqtSignal, QObject
from numpy import double
import numpy as np
from threading import Thread, Lock


class PlotModel(QObject):
    on_total_lum_received_signal = pyqtSignal(bool)
    on_raw_data_received_signal = pyqtSignal(bool)
    on_lum_per_bunch_received_signal = pyqtSignal(bool)
    on_lum_per_channel_received_signal = pyqtSignal(bool)
    on_lum_per_bunch_before_mask_received_signal = pyqtSignal(bool)

    def __init__(self, device, japc):
        super().__init__()
        self.device = device
        self.japc = japc
        self.mutex = Lock()
        self.raw_data_selection = 0
        self.luminosity_channel_selection = 0
        self.fields = ["ExpertAcquisition#rawData", "ExpertAcquisition#luminosityPerChannel",
                       "LuminosityPerBunch#luminosityPerBunch", "LuminosityBunchSum#totalLuminosityBunchSum",
                       "LuminosityPerBunch#perBunchBeforeMasking", "BunchPatternMask#bunchPatternMask",
                       "ExpertAcquisition#bunchMarkPositions"]
        self.ag_fields = ["ExpertSetting#AGStart", "ExpertSetting#AGLength"]
        self.raw_data_channel_field = "ExpertSetting#rawDataChannel"

        self.raw_data = []
        self.raw_data2 = []

        self.bunch_mark_positions = []
        self.luminosity_per_channel_data = None
        self.luminosity_per_bunch_data = None
        self.total_lum_data = None
        self.mask = None
        self.luminosity_per_bunch_before_mask_data = None
        self.ag_start = [0] * 2
        self.ag_length = [0] * 2
        self.ag_curve = [np.array([])] * 2
        self.ag_range = [np.array([])] * 2
        self.ag_range_raw = [np.array([])] * 2
        self.luminosity_per_channel_data_complete = None

    def on_received(self, parameterName, dictValues):
        self.get_ag_settings()
        if str.__contains__(parameterName, "totalLuminosityBunchSum"):
            self.total_lum_data = dictValues
            self.on_total_lum_received_signal.emit(True)

        elif str.__contains__(parameterName, "rawData"):
            self.raw_data = (dictValues[0])
            self.raw_data2 = (dictValues[1])
            self.bunch_mark_positions = self.japc.getParam("{}/{}".format(self.device, self.fields[6]))[
                self.raw_data_selection]
            self.on_raw_data_received_signal.emit(True)

        elif str.__contains__(parameterName, "luminosityPerBunch"):
            self.luminosity_per_bunch_data = dictValues

            self.on_lum_per_bunch_received_signal.emit(True)

        elif str.__contains__(parameterName, "luminosityPerChannel"):
            self.luminosity_per_channel_data = dictValues[self.luminosity_channel_selection]
            self.on_lum_per_channel_received_signal.emit(True)

        elif str.__contains__(parameterName, "perBunchBeforeMasking"):
            self.luminosity_per_bunch_before_mask_data = dictValues
            mask = self.japc.getParam("{}/{}".format(self.device, self.fields[5]))

            for i in range(len(mask)):
                mask[i] = double(mask[i] * 1)
            self.mask = np.array(mask).astype(type(self.luminosity_per_bunch_data[0])) * (max(
                self.luminosity_per_bunch_before_mask_data) + 1)
            self.on_lum_per_bunch_before_mask_received_signal.emit(True)

    def get_all(self):
        self.get_ag_settings()

        self.raw_data = self.japc.getParam("{}/{}".format(self.device, self.fields[0]))[0]
        self.raw_data2 = self.japc.getParam("{}/{}".format(self.device, self.fields[0]))[1]

        self.bunch_mark_positions = self.japc.getParam("{}/{}".format(self.device, self.fields[6]))[
            self.raw_data_selection]
        self.on_raw_data_received_signal.emit(True)

        self.luminosity_per_channel_data = self.japc.getParam("{}/{}".format(self.device, self.fields[1]))[
            self.luminosity_channel_selection]
        self.luminosity_per_channel_data_complete = self.japc.getParam("{}/{}".format(self.device, self.fields[1]))
        self.on_lum_per_channel_received_signal.emit(True)

        self.total_lum_data = self.japc.getParam("{}/{}".format(self.device, self.fields[3]))

        self.on_total_lum_received_signal.emit(True)

        self.luminosity_per_bunch_data = self.japc.getParam(
            "{}/{}".format(self.device, self.fields[2]))
        self.on_lum_per_bunch_received_signal.emit(True)

        self.luminosity_per_bunch_before_mask_data = self.japc.getParam("{}/{}".format(self.device, self.fields[4]))
        mask = self.japc.getParam("{}/{}".format(self.device, self.fields[5]))

        for i in range(len(mask)):
            mask[i] = double(mask[i] * 1)
        self.mask = np.array(mask).astype(type(self.luminosity_per_bunch_data[0])) * (max(
            self.luminosity_per_bunch_before_mask_data) + 1)
        self.on_lum_per_bunch_before_mask_received_signal.emit(True)

    def create_subscription(self):
        for field in self.fields:
            self.japc.subscribeParam("{}/{}".format(self.device, field),
                                     onValueReceived=self.on_received)
        self.japc.startSubscriptions()

    def delete_all_subscriptions(self):
        for field in self.fields:
            self.japc.stopSubscriptions(parameterName="{}/{}".format(self.device, field))
            self.japc.clearSubscriptions(parameterName="{}/{}".format(self.device, field))

    def set_device(self, device):
        self.delete_subscriptions()
        self.device = device

    def get_ag_settings(self):
        self.mutex.acquire()
        try:
            self.ag_start = self.japc.getParam("{}/{}".format(self.device, self.ag_fields[0]))
            self.ag_length = self.japc.getParam("{}/{}".format(self.device, self.ag_fields[1]))
            for i in range(len(self.ag_start)):
                # convert to nanoseconds
                self.ag_start[i] = self.ag_start[i] * 8
                self.ag_length[i] = self.ag_length[i] * 8

            max_data = 1
            for i in range(len(self.ag_curve)):
                self.ag_curve[i] = np.full(self.ag_length[i], max_data)
                self.ag_range[i] = np.arange(self.ag_start[i], self.ag_start[i] + self.ag_length[i])
        finally:
            self.mutex.release()

    def compute_curve_and_range(self, ag_start, ag_length, divide_by):
        self.mutex.acquire()
        try:
            ag_start_local = np.copy(ag_start)
            ag_length_local = np.copy(ag_length)
            ag_range = [np.array([])] * 2
            ag_curve = [np.array([])] * 2

            for i in range(2):
                ag_length_local[i] = ag_length_local[i] / divide_by
                ag_start_local[i] = ag_start_local[i] / divide_by

                ag_curve[i] = np.full(ag_length_local[i], 1)
                ag_range[i] = np.arange(ag_start_local[i], ag_start_local[i] + ag_length_local[i])
        finally:
            self.mutex.release()
        return ag_curve, ag_range

    def get_raw_data_channel(self):
        return self.japc.getParam("{}/{}".format(self.device, self.raw_data_channel_field))
