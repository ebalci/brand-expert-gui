from PyQt5.QtCore import pyqtSignal, QObject
from numpy import int_, double

from brand_expert_gui.constants import TIMING_ENUM, WORKING_MODE_ENUM, EMULATOR_TIMING_ENUM, INSTRUMENT_MODE_ENUM


class ExpertSettingModel(QObject):
    on_settings_received_signal = pyqtSignal(bool)

    def __init__(self, device, japc):
        super().__init__()
        self.device = device
        self.japc = japc

        self.raw_data_selection = 0
        self.luminosity_channel_selection = 0
        self.field = "ExpertSetting"
        version_smaller_then_18 = False

        if version_smaller_then_18:
            self.single_fields = ["workingMode", "cumulativeTurns", "countingThreshold", "rawDataChannel", "timing",
                                  "perBunchPeriod", "bunchSumPeriod", "emulatorPeriod", "emulatorLength",
                                  "emulatorTiming",
                                  "voltageLunSelector", "fineDelay", "insturmentMode", "LEDDelay", "LEDLength"]
            self.double_fields = ["AGLength", "AGStart", "AGdisabled", "bstDelay"]
            self.single_types = [str, int, double, int, str, int, int, int, int, str, int, int, str, int, int]
            self.double_types = [int_, int_, int_, int_]
        else:
            self.single_fields = ["workingMode", "cumulativeTurns", "countingThreshold", "rawDataChannel", "timing",
                                  "perBunchPeriod", "bunchSumPeriod", "emulatorPeriod", "emulatorLength",
                                  "emulatorTiming",
                                  "voltageLunSelector", "insturmentMode", "LEDDelay", "LEDLength", "maskExtentionRight",
                                  "maskExtentionLeft"]
            self.double_fields = ["AGLength", "AGStart", "AGdisabled", "bstDelay", "fineDelay"]
            self.single_types = [str, int, double, int, str, int, int, int, int, str, int, str, int, int, int, int]
            self.double_types = [int_, int_, bool, int_, int]
        self.multi_fields = ["channelSelection", "DAQVoltage", "dataPolarityInversion"]

        self.multi_types = [bool, double, bool]
        self.result_single = []
        self.result_double = []
        self.result_multi = []

    def get_all(self):
        params = self.japc.getParam("{}/{}".format(self.device, self.field))
        self.result_single = []
        self.result_double = []
        self.result_multi = []
        for i in self.single_fields:
            if type(params[i]) == tuple:
                self.result_single.append([i, params[i][1]])
            else:
                self.result_single.append([i, params[i]])
        for i in self.double_fields:
            temp = [i]
            for par in params[i]:
                temp.append(par)
            self.result_double.append(temp)
        for i in self.multi_fields:
            temp = [i]
            for par in params[i]:
                temp.append(par)
            self.result_multi.append(temp)
        self.on_settings_received_signal.emit(True)

    def set_device(self, device):
        self.device = device

    def set(self, data_single, data_double, data_multi):
        # pyjapc won't accept str, convert them to int
        data_single['workingMode'] = WORKING_MODE_ENUM[data_single['workingMode']]
        data_single['timing'] = TIMING_ENUM[data_single['timing']]
        data_single['emulatorTiming'] = EMULATOR_TIMING_ENUM[data_single['emulatorTiming']]
        print(data_single['insturmentMode'])
        data_single['insturmentMode'] = INSTRUMENT_MODE_ENUM[data_single['insturmentMode']]
        data_single.update(data_double)
        data_single.update(data_multi)
        self.japc.setParam("{}/{}".format(self.device, self.field), data_single, checkDims=False)
