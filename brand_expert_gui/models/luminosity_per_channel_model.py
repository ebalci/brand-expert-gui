from PyQt5.QtCore import pyqtSignal, QObject


class LuminosityPerChannelModel(QObject):
    on_lum_per_channel_received_signal = pyqtSignal(bool)

    def __init__(self, device, japc):
        super().__init__()
        self.device = device
        self.japc = japc
        self.fields = ["Acquisition#luminosityPerChannel"]
        self.luminosity_per_channel_data = None

    def on_received(self, parameterName, dictValues):
        if str.__contains__(parameterName, "luminosityPerChannel"):
            self.luminosity_per_channel_data = dictValues
            self.on_lum_per_channel_received_signal.emit(True)

    def get(self):
        self.luminosity_per_channel_data = self.japc.getParam("{}/{}".format(self.device, self.fields[0]))
        self.on_lum_per_channel_received_signal.emit(True)

    def create_subscription(self):
        for field in self.fields:
            self.japc.subscribeParam("{}/{}".format(self.device, field),
                                     onValueReceived=self.on_received)
        self.japc.startSubscriptions()

    def delete_all_subscriptions(self):
        for field in self.fields:
            self.japc.stopSubscriptions(parameterName="{}/{}".format(self.device, field))
            self.japc.clearSubscriptions(parameterName="{}/{}".format(self.device, field))

    def set_device(self, device):
        self.delete_subscriptions()
        self.device = device
