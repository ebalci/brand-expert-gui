import time
import numpy as np
from PyQt5.QtCore import QObject


class SolenoidModel(QObject):

    def __init__(self, device1, device2, japc):
        super().__init__()
        self.device1 = device1
        self.device2 = device2
        self.japc = japc
        self.luminosity_channel_selection = 0
        self.field = "Setting#channelState"
        self.off = np.array([0, 0, 0, 0, 0, 0])
        self.full_transmission = [np.array([1, 1, 1, 1, 0, 0]), np.array([0, 0, 0, 0, 0, 0])]
        self.ten_percent_transmission = [np.array([0, 1, 0, 1, 1, 0]), np.array([0, 1, 0, 0, 0, 0])]
        self.one_percent_transmission = [np.array([0, 0, 0, 0, 1, 0]), np.array([1, 1, 1, 0, 0, 0])]
        print(self.japc.getParam("{}/{}".format(self.device1, self.field)))

    def set_full_transmission(self):
        self.set_transmission(self.full_transmission[0], self.full_transmission[1])

    def set_10_percent_transmission(self):
        self.set_transmission(self.ten_percent_transmission[0], self.ten_percent_transmission[1])

    def set_1_percent_transmission(self):
        self.set_transmission(self.one_percent_transmission[0], self.one_percent_transmission[1])

    def turn_off(self):
        self.set_transmission(self.off, self.off)

    def set_transmission(self, ch1, ch2):
        print(ch1, ch2)
        self.japc.setParam("{}/{}".format(self.device1, self.field), ch1)
        self.japc.setParam("{}/{}".format(self.device2, self.field), ch2)
        time.sleep(1)
        self.japc.setParam("{}/{}".format(self.device1, self.field), self.off)
        self.japc.setParam("{}/{}".format(self.device2, self.field), self.off)
        print(self.japc.getParam("{}/{}".format(self.device1, self.field)))
