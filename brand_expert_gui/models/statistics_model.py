import math
import time

from PyQt5.QtCore import pyqtSignal, QObject
import numpy as np

from brand_expert_gui.constants import CHANNEL_NUMBER, ADC_NUMBER


def timer(method):
    def wrapper(*args, **kw):
        starting = int(round(time.time() * 1000))
        ret = method(*args, **kw)
        ending = int(round(time.time() * 1000))
        print(ending - starting, 'ms')
        return ret

    return wrapper


def compute_range(array):
    """
    Computes the range by (mean - 5 std deviation) to (mean + 5 std deviation)
    :param array: any array of int, double or array of arrays
    :return: min and max for the range
    """
    sigma = np.std(array)
    mean = np.mean(array)
    min = mean - 5 * sigma
    max = mean + 5 * sigma
    return int(min), int(max)


@timer
def get_range(array):
    if len(array) == CHANNEL_NUMBER or len(array) == ADC_NUMBER:
        # array is an array of arrays.
        min_final = int(max(array[0]))
        max_final = int(min(array[0]))
        for arr in array:
            min_local, max_local = compute_range(arr)
            min_final = min(min_final, min_local)
            max_final = max(max_final, max_local)
        return min_final, max_final

    else:
        return compute_range(array)


class StatisticsModel(QObject):
    on_total_lum_received_signal = pyqtSignal(bool)
    on_raw_data_received_signal = pyqtSignal(bool)
    on_lum_per_bunch_received_signal = pyqtSignal(bool)
    on_lum_per_channel_received_signal = pyqtSignal(bool)
    on_lum_per_bunch_before_mask_received_signal = pyqtSignal(bool)

    def __init__(self, device, japc):
        super().__init__()
        self.device = device
        self.japc = japc

        self.raw_data_selection = 0
        self.luminosity_channel_selection = 0
        self.fields = ["ExpertAcquisition#rawData", "ExpertAcquisition#luminosityPerChannel",
                       "LuminosityPerBunch#luminosityPerBunch"]
        self.raw_data_channel_field = "ExpertSetting#rawDataChannel"
        self.raw_data_unique = [np.array([])] * 2
        self.raw_data_count = [np.array([])] * 2
        self.luminosity_per_channel_data_unique = [np.array([])] * 8
        self.luminosity_per_channel_data_count = [np.array([])] * 8
        self.luminosity_per_bunch_data_unique = np.array([])
        self.luminosity_per_bunch_data_count = np.array([])

        self.unique = [self.raw_data_unique, self.luminosity_per_channel_data_unique,
                       self.luminosity_per_bunch_data_unique]
        self.count = [self.raw_data_count, self.luminosity_per_channel_data_count, self.luminosity_per_bunch_data_count]
        self.raw_data_stdev = [0] * 2
        self.luminosity_per_channel_data_stdev = [0] * 8
        self.luminosity_per_bunch_data_stdev = 0

        self.plot_refresh_freq = 10
        self.counter = [self.plot_refresh_freq, self.plot_refresh_freq, self.plot_refresh_freq]
        self.number_of_bins = [10000, 10000, 10000]
        self.selected_channels = [False] * 8
        self.range_setted = [False] * 3

    def on_received(self, parameterName, dictValues):
        if str.__contains__(parameterName, "rawData"):
            if self.range_setted[0]:
                for i in range(dictValues.shape[0]):
                    self.create_histogram(0, dictValues[i], channel_index=i)
            else:
                min_range, max_range = get_range(dictValues)
                print(min_range, max_range)
                for i in range(dictValues.shape[0]):
                    self.create_histogram(0, dictValues[i], channel_index=i, hist_range=(min_range, max_range))
                self.range_setted[0] = True

            if self.counter[0] == self.plot_refresh_freq:
                self.counter[0] = 0
                for i in range(len(self.raw_data_unique)):
                    self.raw_data_stdev[i] = self.get_stddev(self.unique[0][i], self.count[0][i])
                self.on_raw_data_received_signal.emit(True)
            else:
                self.counter[0] = self.counter[0] + 1

        elif str.__contains__(parameterName, "luminosityPerChannel"):
            if self.range_setted[1]:
                for i in range(dictValues.shape[0]):
                    if self.selected_channels[i]:
                        if dictValues[i][len(dictValues[i]) - 1] == 0:
                            self.create_histogram(1, np.delete(dictValues[i], -1).astype(int), channel_index=i)
                        self.create_histogram(1, dictValues[i].astype(int), channel_index=i)
            else:
                min_range, max_range = get_range(dictValues)
                print(min_range, max_range)
                for i in range(dictValues.shape[0]):
                    if self.selected_channels[i]:
                        if dictValues[i][len(dictValues[i]) - 1] == 0:
                            self.create_histogram(1, np.delete(dictValues[i], -1).astype(int), channel_index=i,
                                                  hist_range=(min_range, max_range))
                        self.create_histogram(1, dictValues[i].astype(int), channel_index=i,
                                              hist_range=(min_range, max_range))
                self.range_setted[1] = True

            if self.counter[1] == self.plot_refresh_freq:
                self.counter[1] = 0
                for i in range(len(self.luminosity_per_channel_data_unique)):
                    self.luminosity_per_channel_data_stdev[i] = self.get_stddev(self.unique[1][i], self.count[1][i])
                self.on_lum_per_channel_received_signal.emit(True)
            else:
                self.counter[1] = self.counter[1] + 1

        elif str.__contains__(parameterName, "luminosityPerBunch"):
            if self.range_setted[2]:
                self.create_histogram(2, dictValues.astype(int))
            else:
                min_range, max_range = get_range(dictValues.astype(int))
                self.create_histogram(2, dictValues.astype(int), hist_range=(min_range, max_range))
                self.range_setted[2] = True
                print(min_range, max_range)

            if self.counter[2] == self.plot_refresh_freq:
                self.counter[2] = 0
                self.luminosity_per_bunch_data_stdev = self.get_stddev(self.unique[2], self.count[2])
                self.on_lum_per_bunch_received_signal.emit(True)
            else:
                self.counter[2] = self.counter[2] + 1

    def create_subscription(self):
        for field in self.fields:
            self.japc.subscribeParam("{}/{}".format(self.device, field),
                                     onValueReceived=self.on_received)
        self.japc.startSubscriptions()

    def create_subscription_field(self, index):
        self.japc.subscribeParam("{}/{}".format(self.device, self.fields[index]),
                                 onValueReceived=self.on_received)
        self.japc.startSubscriptions()

    def delete_subscription_field(self, index):
        self.japc.stopSubscriptions(parameterName="{}/{}".format(self.device, self.fields[index]))
        self.japc.clearSubscriptions(parameterName="{}/{}".format(self.device, self.fields[index]))

    def delete_all_subscriptions(self):
        for field in self.fields:
            self.japc.stopSubscriptions(parameterName="{}/{}".format(self.device, field))
            self.japc.clearSubscriptions(parameterName="{}/{}".format(self.device, field))

    def set_device(self, device):
        self.delete_subscriptions()
        self.device = device

    def clear_field(self, index):
        if index == 0:
            self.raw_data_unique = [np.array([])] * 2
            self.raw_data_count = [np.array([])] * 2
            self.raw_data_stdev = [0] * 2
            self.unique[0] = self.raw_data_unique
            self.count[0] = self.raw_data_count
            self.range_setted[0] = False
        elif index == 1:
            self.luminosity_per_channel_data_unique = [np.array([])] * 8
            self.luminosity_per_channel_data_count = [np.array([])] * 8
            self.luminosity_per_channel_data_stdev = [0] * 8
            self.unique[1] = self.luminosity_per_channel_data_unique
            self.count[1] = self.luminosity_per_channel_data_count
            self.range_setted[1] = False

        elif index == 2:
            self.luminosity_per_bunch_data_unique = np.array([])
            self.luminosity_per_bunch_data_count = np.array([])
            self.luminosity_per_bunch_data_stdev = 0
            self.unique[2] = self.luminosity_per_bunch_data_unique
            self.count[2] = self.luminosity_per_bunch_data_count
            self.range_setted[2] = False

    def get_mean_value(self, unique, count):
        total = 0
        for i in range(len(unique)):
            total = total + unique[i] * count[i]
        return total / np.sum(count)

    def get_stddev(self, unique, count):
        if len(unique) == 0 or len(count) == 0:
            return None
        mean = self.get_mean_value(unique, count)
        stddev = 0

        for i in range(len(unique)):
            stddev = stddev + count[i] * pow((unique[i] - mean), 2)
        stddev = round(math.sqrt(stddev * (1 / np.sum(count))), 5)
        return stddev

    def create_histogram(self, index, array, channel_index=None, hist_range=None):
        if channel_index is None:
            if self.unique[index].size == 0:
                if hist_range is not None:
                    counts, bins = np.histogram(array, bins=self.number_of_bins[index],
                                                range=hist_range)
                else:
                    counts, bins = np.histogram(array, bins=self.number_of_bins[index])

                self.count[index] = counts
                self.unique[index] = np.delete(bins, -1)
            else:
                counts, bins = np.histogram(array, bins=self.unique[index])
                self.count[index] = np.add(np.insert(counts, -1, 0), self.count[index])
        else:
            if self.unique[index][channel_index].size == 0:
                if hist_range is not None:
                    counts, bins = np.histogram(array, bins=self.number_of_bins[index],
                                                range=hist_range)
                else:
                    counts, bins = np.histogram(array, bins=self.number_of_bins[index])

                self.count[index][channel_index] = counts
                self.unique[index][channel_index] = np.delete(bins, -1)
            else:
                counts, bins = np.histogram(array, bins=self.unique[index][channel_index])
                self.count[index][channel_index] = np.add(np.insert(counts, -1, 0), self.count[index][channel_index])

    def get_raw_data_channel(self):
        return self.japc.getParam("{}/{}".format(self.device, self.raw_data_channel_field))
