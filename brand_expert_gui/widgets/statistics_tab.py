import time
from typing import Iterable

from PyQt5.QtWidgets import QWidget
import pyqtgraph as pg

from brand_expert_gui.constants import CHANNEL_NUMBER, ADC_NUMBER, COLORS
from brand_expert_gui.models.statistics_model import StatisticsModel
from brand_expert_gui.widgets.resources.generated.ui_statistics_tab import Ui_TabWidget


class StatisticsTab(QWidget, Ui_TabWidget):

    def __init__(self, main_widget, parent, device):
        super(StatisticsTab, self).__init__(parent)

        # Setup itself as the view
        self.setupUi(parent)

        self.main_widget = main_widget
        self.device = device
        self.japc = main_widget.japc
        self.model = None
        self.init_model()
        self.active = False

        self.histogram_plots = [self.plot_raw_data_hist, self.plot_luminosity_per_channel_hist,
                                self.plot_luminosity_per_bunch_hist]

        self.curve = [[], [], [], [], [], []]
        self.bar_graph = [[[], []], [], [[], [], [], [], [], [], [], []]]
        self.button_start.pressed.connect(self.start_pressed)
        self.button_stop.pressed.connect(self.stop_pressed)
        self.button_clear.pressed.connect(self.clear_pressed)

        self.button_start_raw_data.pressed.connect(lambda: self.start_field_pressed(0))
        self.button_start_per_channel.pressed.connect(lambda: self.start_field_pressed(1))
        self.button_start_per_bunch.pressed.connect(lambda: self.start_field_pressed(2))

        self.button_stop_raw_data.pressed.connect(lambda: self.stop_field_pressed(0))
        self.button_stop_per_channel.pressed.connect(lambda: self.stop_field_pressed(1))
        self.button_stop_per_bunch.pressed.connect(lambda: self.stop_field_pressed(2))

        self.button_clear_raw_data.pressed.connect(lambda: self.clear_field_pressed(0))
        self.button_clear_per_channel.pressed.connect(lambda: self.clear_field_pressed(1))
        self.button_clear_per_bunch.pressed.connect(lambda: self.clear_field_pressed(2))
        self.check_box = [self.checkBox_0, self.checkBox_1, self.checkBox_2, self.checkBox_3, self.checkBox_4,
                          self.checkBox_5, self.checkBox_6, self.checkBox_7]
        self.check_box_raw = [self.checkBox_raw_0, self.checkBox_raw_1]
        self.stdev_raw_data = [self.textedit_raw_data_stdev_0, self.textedit_raw_data_stdev_1]
        self.stdev_per_channel = [self.textedit_per_channel_stdev_0, self.textedit_per_channel_stdev_1,
                                  self.textedit_per_channel_stdev_2, self.textedit_per_channel_stdev_3,
                                  self.textedit_per_channel_stdev_4, self.textedit_per_channel_stdev_5,
                                  self.textedit_per_channel_stdev_6, self.textedit_per_channel_stdev_7]
        self.stdev = [self.stdev_raw_data, self.stdev_per_channel, self.textedit_luminosity_per_bunch_stdev]
        self.checkBox_all.stateChanged.connect(self.check_all)
        self.setup_checkbox()
        self.channel_selected = [False] * 8

    def setup_checkbox(self):
        for i in range(CHANNEL_NUMBER):
            self.check_box[i].setStyleSheet("QCheckBox::indicator:checked"
                                            "{"
                                            "background-color :" + COLORS[i] + ";"
                                                                               "}")
            self.check_box[i].stateChanged.connect(self.checkbox_pressed)
        for i in range(ADC_NUMBER):
            self.check_box_raw[i].setStyleSheet("QCheckBox::indicator:checked"
                                                "{"
                                                "background-color :" + COLORS[i] + ";"
                                                                                   "}")

    def set_new_device(self, device):
        self.clear_pressed()

        self.device = device
        for i in range(ADC_NUMBER):
            self.plot_raw_data_hist.removeItem(self.bar_graph[0][i])
        self.plot_luminosity_per_bunch_hist.removeItem(self.bar_graph[1])
        for i in range(CHANNEL_NUMBER):
            self.plot_luminosity_per_channel_hist.removeItem(self.bar_graph[2][i])
        self.curve = [[], [], [], [], [], []]
        self.bar_graph = [[[], []], [], [[], [], [], [], [], [], [], []]]

        self.model.delete_all_subscriptions()
        self.active = False
        self.init_model()
        self.checkbox_pressed()

    def init_model(self):
        self.model = StatisticsModel(self.device, self.japc)

        self.model.on_raw_data_received_signal.connect(self.update_raw_data)
        self.model.on_lum_per_bunch_received_signal.connect(self.update_per_bunch)
        self.model.on_lum_per_channel_received_signal.connect(self.update_per_channel)

    def update_raw_data(self):
        raw_data_ch = self.model.get_raw_data_channel()
        self.label_raw_data_channel_selection.setText(str(raw_data_ch))

        for i in range(ADC_NUMBER):
            self.plot_raw_data_hist.removeItem(self.bar_graph[0][i])
            if self.check_box_raw[i].isChecked():
                self.bar_graph[0][i] = pg.BarGraphItem(x=self.model.unique[0][i], height=self.model.count[0][i],
                                                       width=0.1, pen=pg.mkPen(color=COLORS[raw_data_ch + 4 * i]))
                self.plot_raw_data_hist.addItem(self.bar_graph[0][i])
                self.set_stdev(self.stdev_raw_data, self.model.raw_data_stdev)
                self.check_box_raw[i].setStyleSheet("QCheckBox::indicator:checked"
                                                    "{"
                                                    "background-color :" + COLORS[raw_data_ch + 4 * i] + ";"
                                                                                                         "}")

    def update_per_bunch(self):
        self.plot_luminosity_per_bunch_hist.removeItem(self.bar_graph[1])
        self.textedit_luminosity_per_bunch_stdev.setText(str(self.model.luminosity_per_bunch_data_stdev))
        self.bar_graph[1] = pg.BarGraphItem(x=self.model.unique[2], height=self.model.count[2], width=0.1)
        self.plot_luminosity_per_bunch_hist.addItem(self.bar_graph[1])

    def update_per_channel(self):
        for i in range(CHANNEL_NUMBER):
            self.plot_luminosity_per_channel_hist.removeItem(self.bar_graph[2][i])
            if self.check_box[i].isChecked():
                self.bar_graph[2][i] = pg.BarGraphItem(x=self.model.unique[1][i], height=self.model.count[1][i],
                                                       width=0.1,
                                                       pen=pg.mkPen(color=COLORS[i]))
                self.plot_luminosity_per_channel_hist.addItem(self.bar_graph[2][i])
                self.set_stdev(self.stdev_per_channel, self.model.luminosity_per_channel_data_stdev)

    def start_pressed(self):
        self.set_bin_numbers()
        self.model.create_subscription()
        self.active = True
        self.checkbox_pressed()

    def stop_pressed(self):
        print("stop pressed")
        self.model.delete_all_subscriptions()
        self.active = False

    def clear_pressed(self):
        for i in range(3):
            self.clear_field_pressed(i)
        for plot in self.histogram_plots:
            plot.clear()
        self.init_model()

    def start_field_pressed(self, index):
        self.set_bin_numbers()
        self.model.create_subscription_field(index)
        self.checkbox_pressed()

    def stop_field_pressed(self, index):
        self.model.delete_subscription_field(index)

    def clear_field_pressed(self, index):
        self.model.clear_field(index)
        self.histogram_plots[index].clear()
        if isinstance(self.stdev[index], Iterable):
            for i in self.stdev[index]:
                i.setText("None")
        else:
            self.stdev[index].setText("None")
        print("clear ", index)

    def checkbox_pressed(self):
        for index in range(CHANNEL_NUMBER):
            self.model.selected_channels[index] = self.check_box[index].isChecked()

    def set_bin_numbers(self):
        self.model.number_of_bins = [int(self.textedit_raw_data_binnumber.text()),
                                     int(self.textedit_lum_per_channel_binnumber.text()),
                                     int(self.textedit_lum_per_bunch_binnumber.text())]

    def set_stdev(self, textedits, stdevs):
        try:
            for i in range(len(stdevs)):
                textedits[i].setText(str(stdevs[i]))
        except Exception:
            print("Failed to publish standard deviation")

    def check_all(self):
        for box in self.check_box:
            box.setChecked(self.checkBox_all.isChecked())


