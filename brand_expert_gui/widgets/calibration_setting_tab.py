from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget
from numpy import double

from brand_expert_gui.constants import CALIBRATION_SETTING_HEADERS, SOLENOID_DICT, SOLENOID_B_DICT, \
    AMPLIFIER_OFFSET_HEADERS, AMPLIFIER_GAIN_HEADERS
from brand_expert_gui.models.calibration_setting_model import CalibrationSettingModel
from brand_expert_gui.models.solenoid_model import SolenoidModel
from brand_expert_gui.utils import setup_table, get_table_data
from brand_expert_gui.widgets.resources.generated.ui_calibration_setting import Ui_TabWidget


class CalibrationSettingTab(QWidget, Ui_TabWidget):

    def __init__(self, main_widget, parent, device):
        super(CalibrationSettingTab, self).__init__(parent)

        # Setup itself as the view
        self.setupUi(parent)

        self.main_widget = main_widget
        self.device = device
        self.japc = main_widget.japc
        self.model = None
        self.solenoids_model = None
        self.sol1 = None
        self.sol2 = None
        self.set_new_device(self.device)
        self.button_get.pressed.connect(self.get_pressed)
        self.button_set.pressed.connect(self.set_pressed)
        self.button_get_amplifier.pressed.connect(self.get_amplifier_pressed)
        self.button_set_amplifier.pressed.connect(self.set_amplifier_pressed)
        self.button_set_solenoid.pressed.connect(self.solenoid_set_pressed)

    def set_new_device(self, device):
        self.device = device
        self.model = CalibrationSettingModel(device, self.japc)
        self.init_model()
        self.table.clear()
        self.amplifierOffsetTable.clear()
        self.amplifierGainTable.clear()

        try:
            self.sol1 = SOLENOID_DICT[self.device]
            self.sol2 = SOLENOID_B_DICT[self.device]
            self.solenoids_model = SolenoidModel(self.sol1, self.sol2, self.japc)
            self.groupBox_sol1.setTitle(self.sol1 + ", " + self.sol2)
        except Exception as e:
            self.groupBox_sol1.setTitle("UNAVAILABLE!")
            print(e)

    def init_model(self):
        self.model = CalibrationSettingModel(self.device, self.japc)
        self.model.on_settings_received_signal.connect(self.update_table)
        self.model.on_amplifier_settings_received_signal.connect(self.update_amplifier_tables)

        self.table.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.amplifierOffsetTable.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.amplifierGainTable.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents)

    def update_table(self):
        setup_table(self.table, self.model.result)
        print(self.model.result)
        self.table.setHorizontalHeaderLabels(CALIBRATION_SETTING_HEADERS)

    def update_amplifier_tables(self):
        setup_table(self.amplifierGainTable, [[self.model.amplifier_fields[0], self.model.amplifier_gain]])
        setup_table(self.amplifierOffsetTable, [[self.model.amplifier_fields[1], *self.model.amplifier_offset]])
        self.amplifierOffsetTable.setHorizontalHeaderLabels(AMPLIFIER_OFFSET_HEADERS)
        self.amplifierGainTable.setHorizontalHeaderLabels(AMPLIFIER_GAIN_HEADERS)

    def get_pressed(self):
        if self.model is not None:
            self.model.get_calibration()

    def get_amplifier_pressed(self):
        if self.model is not None:
            self.model.get_amplifier()

    def set_pressed(self):
        self.model.set(get_table_data(self.table, self.model.types))

    def solenoid_set_pressed(self):
        if self.radioButton_sol1_1.isChecked():
            self.solenoids_model.set_1_percent_transmission()
        elif self.radioButton_sol1_10.isChecked():
            self.solenoids_model.set_10_percent_transmission()
        elif self.radioButton_sol1_100.isChecked():
            self.solenoids_model.set_full_transmission()

    def set_amplifier_pressed(self):
        dict_setting = get_table_data(self.amplifierOffsetTable, [double] * 8)
        dict_setting[str(self.amplifierGainTable.item(0,0).text())] = double(
            self.amplifierGainTable.item(0,1).text())
        for x in dict_setting:
            print(str(x))
        self.model.set_amplifier(dict_setting)
