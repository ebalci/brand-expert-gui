from PyQt5.QtWidgets import QWidget
import pyqtgraph as pg

from brand_expert_gui.constants import SUBSCRIBE, UNSUBSCRIBE, CHANNEL_NUMBER, COLORS
from brand_expert_gui.image_utils import generate_image
from brand_expert_gui.models.luminosity_per_channel_model import LuminosityPerChannelModel
from brand_expert_gui.widgets.resources.generated.ui_luminosity_per_channel import Ui_TabWidget


class LuminosityPerChannelTab(QWidget, Ui_TabWidget):

    def __init__(self, main_widget, parent, device):
        super(LuminosityPerChannelTab, self).__init__(parent)

        # Setup itself as the view
        self.setupUi(parent)

        self.main_widget = main_widget
        self.device = device
        self.japc = main_widget.japc

        self.model = None
        self.button_get.pressed.connect(self.get_pressed)
        self.button_subscribe.pressed.connect(self.subscribe_pressed)
        self.curve = [[], [], [], [], [], [], [], []]
        self.image = None

        self.checkBox_all.setChecked(True)
        self.check_box = [self.checkBox_0, self.checkBox_1, self.checkBox_2, self.checkBox_3, self.checkBox_4,
                          self.checkBox_5, self.checkBox_6, self.checkBox_7]
        self.setup_checkbox()
        self.checkBox_all_clicked()
        self.init_model()

    def set_new_device(self, device):
        self.device = device
        self.image = None
        for curve in self.curve:
            self.plot_luminosity_per_channel.removeItem(curve)
        self.plot_image_luminosity_per_channel.removeItem(self.image)
        self.model.delete_all_subscriptions()
        self.button_subscribe.setText(SUBSCRIBE)
        self.model = LuminosityPerChannelModel(device, self.japc)
        self.init_model()

    def init_model(self):
        self.model = LuminosityPerChannelModel(self.device, self.japc)
        self.model.on_lum_per_channel_received_signal.connect(self.update_per_channel)

    def update_per_channel(self):
        self.plot_image_luminosity_per_channel.removeItem(self.image)
        for i in range(CHANNEL_NUMBER):
            self.plot_luminosity_per_channel.removeItem(self.curve[i])
            if self.check_box[i].isChecked():
                self.curve[i] = pg.PlotCurveItem(self.model.luminosity_per_channel_data[i],
                                                 pen=pg.mkPen(color=COLORS[i]))
                self.plot_luminosity_per_channel.addItem(self.curve[i])

        self.image = generate_image(self.model.luminosity_per_channel_data, transpose=True)
        self.plot_image_luminosity_per_channel.addItem(self.image)

    def get_pressed(self):
        if self.model is not None:
            self.model.get()

    def subscribe_pressed(self):
        if self.button_subscribe.text() == SUBSCRIBE:
            self.model.create_subscription()
            self.button_subscribe.setText(UNSUBSCRIBE)
        elif self.button_subscribe.text() == UNSUBSCRIBE:
            self.model.delete_all_subscriptions()
            self.button_subscribe.setText(SUBSCRIBE)

    def setup_checkbox(self):
        for i in range(CHANNEL_NUMBER):
            self.check_box[i].setStyleSheet("QCheckBox::indicator:checked"
                                            "{"
                                            "background-color :" + COLORS[i] + ";"
                                                                               "}")
        self.checkBox_all.clicked.connect(self.checkBox_all_clicked)

    def checkBox_all_clicked(self):
        for box in self.check_box:
            box.setChecked(self.checkBox_all.isChecked())
