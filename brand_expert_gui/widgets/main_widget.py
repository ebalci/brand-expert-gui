"""
For reference, see:
https://acc-py.web.cern.ch/gitlab/bisw-python/pyqt-tutorial/docs/stable/2-project-structure.html#project-name-widgets
https://acc-py.web.cern.ch/gitlab/bisw-python/pyqt-tutorial/docs/stable/2-project-structure.html#project-name-widgets-resources
"""
import logging

import pyjapc
from PyQt5.QtWidgets import QTabWidget

# Import the models
from brand_expert_gui.constants import DEVICE_LIST
from brand_expert_gui.models.models import SpinBoxModel, SinglePointSource

# Import the code generated from the main_widget.ui file
from brand_expert_gui.widgets.calibration_setting_tab import CalibrationSettingTab
from brand_expert_gui.widgets.expert_setting_tab import ExpertSettingTab
from brand_expert_gui.widgets.luminosity_per_channel_tab import LuminosityPerChannelTab
from brand_expert_gui.widgets.plot_tab import PlotTab
from brand_expert_gui.widgets.resources.generated.ui_main_widget import Ui_Form
from brand_expert_gui.widgets.statistics_tab import StatisticsTab


class MainWidget(QTabWidget, Ui_Form):
    """
    This is the main class defining your GUI. In the ModelView architecture, this is the View.

    It loads the GUI definition from Ui_TabWidget (generated code from the main_widget.ui file)

    Qt's signals and slots are usually connected in this class, in the ``__init__`` function.
    The model will usually emit signal which are catch by this class, which translates them into operations
    on the GUI.

    In this example we are connecting the plots on the View with the ``UpdateSource`` classes defined in the `
    `models/`` folder, and the ``SpinBox`` to the control system through the ``SpinBoxModel`` class, that
    performs PyJAPC SET operations.
    """

    def __init__(self, logging, parent=None):
        """
        This function sets up its View, its Model(s) and connects them together.
        :param parent: the owner of this widget. Should be an ApplicationFrame instance. See the Qt Documentation.
        """
        super(MainWidget, self).__init__(parent)

        # Setup itself as the view
        self.setupUi(self)
        self.logging = logging
        self.japc = pyjapc.PyJapc()
        self.japc.setSelector("")
        self.plot_tab = None
        self.expert_setting_tab = None
        self.calibration_setting_tab = None
        self.statistics_tab = None
        self.luminosity_per_channel_tab = None
        self.setup_device_selection()

    def setup_device_selection(self):
        self.comboBox.addItem("")
        for device_name in DEVICE_LIST:
            self.comboBox.addItem(device_name)
        self.comboBox.currentIndexChanged.connect(self.device_selection_changed)

    def device_selection_changed(self, device_index):
        if self.plot_tab is None or self.expert_setting_tab is None:

            if ((device_index is not 1) and (device_index is not 2)) and self.japc.rbacGetToken() is None:
                self.japc.rbacLogin(loginDialog=True)

            if self.device_is_ok(DEVICE_LIST[device_index - 1]):
                self.plot_tab = PlotTab(self, parent=self.tabWidget, device=DEVICE_LIST[device_index - 1])

                self.luminosity_per_channel_tab = LuminosityPerChannelTab(self, parent=self.tabWidget,
                                                                          device=DEVICE_LIST[device_index - 1])

                self.expert_setting_tab = ExpertSettingTab(self, parent=self.tabWidget,
                                                           device=DEVICE_LIST[device_index - 1])
                self.calibration_setting_tab = CalibrationSettingTab(self, parent=self.tabWidget,
                                                                     device=DEVICE_LIST[device_index - 1])
                self.statistics_tab = StatisticsTab(self, parent=self.tabWidget, device=DEVICE_LIST[device_index - 1])
            else:
                logging.exception("Device connection failed. " + str(DEVICE_LIST[device_index - 1]))

        else:
            if device_index is not 1 and device_index is not 2 and self.japc.rbacGetToken() is None:
                self.japc.rbacLogin(loginDialog=True)
            self.plot_tab.close()
            self.expert_setting_tab.close()
            self.calibration_setting_tab.close()
            self.statistics_tab.close()
            self.luminosity_per_channel_tab.close()
            self.plot_tab.set_new_device(DEVICE_LIST[device_index - 1])
            self.expert_setting_tab.set_new_device(DEVICE_LIST[device_index - 1])
            self.calibration_setting_tab.set_new_device(DEVICE_LIST[device_index - 1])
            self.statistics_tab.set_new_device(DEVICE_LIST[device_index - 1])
            self.luminosity_per_channel_tab.set_new_device(DEVICE_LIST[device_index - 1])

    def device_is_ok(self, device_name):
        try:
            acq = self.japc.getParam("{}/{}".format(device_name, "ExpertAcquisition#luminosityPerChannel"))
            if acq is not None:
                return True
            else:
                print("Exception, device is not OK")
                return False
        except Exception as exp:
            print("Exception, device is not OK")
            return False
