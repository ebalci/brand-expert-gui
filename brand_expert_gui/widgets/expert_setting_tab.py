import numpy as np
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QWidget, QComboBox

from brand_expert_gui.constants import EXPERT_SETTINGS_SINGLE_HEADERS, EXPERT_SETTINGS_MULTIPLE_HEADERS, \
    EXPERT_SETTINGS_DOUBLE_HEADERS, WORKING_MODE_ENUM, EMULATOR_TIMING_ENUM, TIMING_ENUM, INSTRUMENT_MODE_ENUM
from brand_expert_gui.models.expert_setting_model import ExpertSettingModel
from brand_expert_gui.utils import setup_table, get_table_data
from brand_expert_gui.widgets.resources.generated.ui_expert_setting import Ui_TabWidget


class ExpertSettingTab(QWidget, Ui_TabWidget):

    def __init__(self, main_widget, parent, device):
        super(ExpertSettingTab, self).__init__(parent)

        # Setup itself as the view
        self.setupUi(parent)

        self.main_widget = main_widget
        self.device = device
        self.japc = main_widget.japc
        self.working_mode_combobox = QComboBox()
        self.timing_combobox = QComboBox()
        self.emulator_timing_combobox = QComboBox()
        self.instrument_mode_combobox = QComboBox()
        # self.solenoid_widget = SolenoidWidget(main_widget, self.tab, "BRANDTEST.SOL", "BRANDTEST.SOLB")
        self.combo_boxes = [self.working_mode_combobox, self.timing_combobox, self.emulator_timing_combobox,
                            self.instrument_mode_combobox]
        self.configure_drop_downs()

        self.model = None
        self.init_model()

        self.button_get.pressed.connect(self.get_pressed)
        self.button_set.pressed.connect(self.set_pressed)
        self.get_pressed()
        self.add_drop_downs()

    def set_new_device(self, device):
        self.device = device
        self.model = ExpertSettingModel(device, self.japc)
        self.table_single.clear()
        self.table_double.clear()
        self.tabl_multiple.clear()
        self.init_model()

    def init_model(self):
        self.model = ExpertSettingModel(self.device, self.japc)
        self.model.on_settings_received_signal.connect(self.update_table)
        self.table_single.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.table_double.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents)

    def update_table(self):
        print("update ", self.model.result_single)
        setup_table(self.table_single, self.model.result_single)
        setup_table(self.table_double, self.model.result_double)
        setup_table(self.tabl_multiple, self.model.result_multi)
        self.table_single.setHorizontalHeaderLabels(EXPERT_SETTINGS_SINGLE_HEADERS)
        self.table_double.setHorizontalHeaderLabels(EXPERT_SETTINGS_DOUBLE_HEADERS)
        self.tabl_multiple.setHorizontalHeaderLabels(EXPERT_SETTINGS_MULTIPLE_HEADERS)
        self.update_drop_downs(self.model.result_single)

    def get_pressed(self):
        if self.model is not None:
            self.model.get_all()

    def set_pressed(self):
        self.model.set(get_table_data(self.table_single, self.model.single_types,
                                      [self.working_mode_combobox, self.timing_combobox, self.emulator_timing_combobox,
                                       self.instrument_mode_combobox]),
                       get_table_data(self.table_double, self.model.double_types),
                       get_table_data(self.tabl_multiple, self.model.multi_types))

    def add_drop_downs(self):
        for i in range(self.table_single.rowCount()):
            if self.table_single.item(i, 0).text() == "workingMode":
                self.table_single.setCellWidget(i, 1, self.working_mode_combobox)
            elif self.table_single.item(i, 0).text() == "timing":
                self.table_single.setCellWidget(i, 1, self.timing_combobox)
            elif self.table_single.item(i, 0).text() == "emulatorTiming":
                self.table_single.setCellWidget(i, 1, self.emulator_timing_combobox)
            elif self.table_single.item(i, 0).text() == "insturmentMode":
                self.table_single.setCellWidget(i, 1, self.instrument_mode_combobox)

    def update_drop_downs(self, result_single):
        self.working_mode_combobox = QComboBox()
        self.timing_combobox = QComboBox()
        self.emulator_timing_combobox = QComboBox()
        self.instrument_mode_combobox = QComboBox()
        self.configure_drop_downs()
        self.add_drop_downs()
        for pair in result_single:
            if pair[0] == 'workingMode':
                self.working_mode_combobox.setCurrentIndex(WORKING_MODE_ENUM[pair[1]])
            elif pair[0] == 'timing':
                self.timing_combobox.setCurrentIndex(TIMING_ENUM[pair[1]])
            elif pair[0] == 'emulatorTiming':
                self.emulator_timing_combobox.setCurrentIndex(EMULATOR_TIMING_ENUM[pair[1]])
            elif pair[0] == 'insturmentMode':
                self.instrument_mode_combobox.setCurrentIndex(INSTRUMENT_MODE_ENUM[pair[1]])

    def configure_drop_downs(self):
        keys = WORKING_MODE_ENUM.keys()
        for key in keys:
            self.working_mode_combobox.addItem(key)
        keys = EMULATOR_TIMING_ENUM.keys()
        for key in keys:
            self.emulator_timing_combobox.addItem(key)
        keys = TIMING_ENUM.keys()
        for key in keys:
            self.timing_combobox.addItem(key)
        keys = INSTRUMENT_MODE_ENUM.keys()
        for key in keys:
            self.instrument_mode_combobox.addItem(key)
