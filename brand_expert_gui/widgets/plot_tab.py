import numpy as np
from PyQt5.QtWidgets import QWidget
import pyqtgraph as pg
from pyqtgraph import mkPen

from brand_expert_gui.constants import CHANNEL_NUMBER, ADC_NUMBER, SUBSCRIBE, UNSUBSCRIBE, COLORS
from brand_expert_gui.models.plot_model import PlotModel
from brand_expert_gui.widgets.resources.generated.ui_plot_tab import Ui_TabWidget


class PlotTab(QWidget, Ui_TabWidget):

    def __init__(self, main_widget, parent, device):
        super(PlotTab, self).__init__(parent)

        # Setup itself as the view
        self.setupUi(parent)

        self.main_widget = main_widget
        self.device = device
        self.japc = main_widget.japc

        self.setup_plot_selection()
        self.model = None
        self.init_model()

        self.curve = [[], [], [], [], [], [], []]
        self.ag_curve = [[[], []], [[], []], [[], []], [[], []]]
        self.button_get.pressed.connect(self.get_pressed)
        self.button_subscribe.pressed.connect(self.subscribe_pressed)
        self.ag_brush_0 = (200, 50, 50, 75)
        self.ag_brush_1 = (50, 200, 50, 75)
        self.checkBox_rawDatacard0.setStyleSheet("QCheckBox::indicator:checked"
                                                 "{"
                                                 "background-color :" + COLORS[3] + ";"
                                                                                    "}")
        self.checkBox_rawDatacard1.setStyleSheet("QCheckBox::indicator:checked"
                                                 "{"
                                                 "background-color :" + COLORS[6] + ";"
                                                                                    "}")
        self.checkBox_rawDatacard0.setChecked(True)
        self.checkBox_rawDatacard1.setChecked(True)

    def setup_plot_selection(self):
        selections = np.arange(ADC_NUMBER)
        for sel in selections:
            self.combobox_raw_data.addItem(str(sel))
        selections = np.arange(CHANNEL_NUMBER)
        for sel in selections:
            self.combobox_luminosity_per_channel.addItem(str(sel))
        self.combobox_luminosity_per_channel.currentIndexChanged.connect(self.lum_per_channel_selection_changed)
        self.combobox_raw_data.currentIndexChanged.connect(self.raw_data_selection_changed)

    def lum_per_channel_selection_changed(self):
        self.model.luminosity_channel_selection = self.combobox_luminosity_per_channel.currentIndex()

    def raw_data_selection_changed(self):
        self.model.raw_data_selection = self.combobox_raw_data.currentIndex()

    def set_new_device(self, device):
        self.device = device
        self.plot_raw_data.removeItem(self.curve[0])
        self.plot_raw_data.removeItem(self.curve[6])
        self.plot_luminosity_per_bunch.removeItem(self.curve[1])
        self.plot_luminosity_per_channel.removeItem(self.curve[2])
        self.plot_luminosity_per_bunch_before_mask.removeItem(self.curve[3])
        self.plot_luminosity_per_bunch_before_mask.removeItem(self.curve[4])
        self.plot_raw_data.removeItem(self.ag_curve[3][0])
        self.plot_raw_data.removeItem(self.ag_curve[3][1])
        self.plot_luminosity_per_bunch.removeItem(self.ag_curve[1][0])
        self.plot_luminosity_per_bunch.removeItem(self.ag_curve[1][1])
        self.plot_luminosity_per_bunch_before_mask.removeItem(self.ag_curve[2][1])
        self.plot_luminosity_per_bunch_before_mask.removeItem(self.ag_curve[2][0])
        self.plot_luminosity_per_channel.removeItem(self.ag_curve[0][0])
        self.plot_luminosity_per_channel.removeItem(self.ag_curve[0][1])
        self.plot_raw_data.removeItem(self.curve[5])
        # //TODO
        self.curve = [[], [], [], [], [], [], []]

        self.lineEdit_total_luminosity_bunch_sum.setText('')
        self.model.delete_all_subscriptions()
        self.button_subscribe.setText(SUBSCRIBE)
        self.model = PlotModel(device, self.japc)
        self.init_model()

    def init_model(self):
        self.model = PlotModel(self.device, self.japc)
        self.model.luminosity_channel_selection = self.combobox_luminosity_per_channel.currentIndex()
        self.model.raw_data_selection = self.combobox_raw_data.currentIndex()

        self.model.on_raw_data_received_signal.connect(self.update_raw_data)
        self.model.on_total_lum_received_signal.connect(self.update_total_lum)
        self.model.on_lum_per_bunch_received_signal.connect(self.update_per_bunch)
        self.model.on_lum_per_channel_received_signal.connect(self.update_per_channel)
        self.model.on_lum_per_bunch_before_mask_received_signal.connect(self.update_per_bunch_before_mask)

    def update_raw_data(self):
        ag_curve, ag_range = self.model.compute_curve_and_range(self.model.ag_start, self.model.ag_length, 2)
        self.plot_raw_data.removeItem(self.ag_curve[3][0])
        self.plot_raw_data.removeItem(self.ag_curve[3][1])

        if self.checkBox_rawDatacard0.isChecked():
            self.add_curve(0, self.plot_raw_data, self.model.raw_data, pen_arg=pg.mkPen(color=COLORS[3]))
        else:
            self.add_curve(0, self.plot_raw_data, self.model.raw_data, pen_arg=mkPen(None))

        if self.checkBox_rawDatacard1.isChecked():
            self.add_curve(6, self.plot_raw_data, self.model.raw_data2, pen_arg=pg.mkPen(color=COLORS[6]))
        else:
            self.add_curve(6, self.plot_raw_data, self.model.raw_data2, pen_arg=mkPen(None))

        scaling_factor = min(self.model.raw_data) if \
            self.model.raw_data[int(ag_range[0][0])] < 0 else max(
            self.model.raw_data)
        self.ag_curve[3][0] = pg.PlotCurveItem(y=ag_curve[0] * scaling_factor,
                                               x=ag_range[0], fillLevel=1,
                                               brush=self.ag_brush_0, pen=mkPen(None), name='Abort Gap Settings 0')
        self.ag_curve[3][1] = pg.PlotCurveItem(y=ag_curve[1] * scaling_factor,
                                               x=ag_range[1], fillLevel=1,
                                               brush=self.ag_brush_1, pen=mkPen(None), name='Abort Gap Settings 1')
        if self.checkBox_abort_gap.isChecked():
            self.plot_raw_data.addItem(self.ag_curve[3][0])
            self.plot_raw_data.addItem(self.ag_curve[3][1])
        if self.checkBox_bunch_marks.isChecked():
            y = [self.model.raw_data[i] for i in self.model.bunch_mark_positions]
            self.add_curve(5, self.plot_raw_data, y, pen_arg=mkPen(None),
                           x=self.model.bunch_mark_positions, symbol='d',
                           symbol_brush=(255, 0, 0, 255))
        else:
            self.plot_raw_data.removeItem(self.curve[5])

        raw_data_ch = self.model.get_raw_data_channel()
        self.channel_selection_label.setText(str(raw_data_ch + 4 * self.combobox_raw_data.currentIndex()))

    def update_total_lum(self):
        self.lineEdit_total_luminosity_bunch_sum.setText(str(self.model.total_lum_data))

    def update_per_bunch(self):
        ag_curve, ag_range = self.model.compute_curve_and_range(self.model.ag_start, self.model.ag_length, 25)
        self.plot_luminosity_per_bunch.removeItem(self.ag_curve[1][0])
        self.plot_luminosity_per_bunch.removeItem(self.ag_curve[1][1])
        self.add_curve(1, self.plot_luminosity_per_bunch, self.model.luminosity_per_bunch_data)
        scaling_factor = min(self.model.luminosity_per_bunch_data) if abs(
            min(self.model.luminosity_per_bunch_data)) > max(self.model.luminosity_per_bunch_data) else max(
            self.model.luminosity_per_bunch_data)

        self.ag_curve[1][0] = pg.PlotCurveItem(y=ag_curve[0] * scaling_factor,
                                               x=ag_range[0], fillLevel=1,
                                               brush=self.ag_brush_0, pen=mkPen(None), name='Abort Gap Settings 0')
        self.ag_curve[1][1] = pg.PlotCurveItem(y=ag_curve[1] * scaling_factor,
                                               x=ag_range[1], fillLevel=1,
                                               brush=self.ag_brush_1, pen=mkPen(None), name='Abort Gap Settings 1')
        self.plot_luminosity_per_bunch.addItem(self.ag_curve[1][0])
        self.plot_luminosity_per_bunch.addItem(self.ag_curve[1][1])

    def update_per_bunch_before_mask(self):
        ag_curve, ag_range = self.model.compute_curve_and_range(self.model.ag_start, self.model.ag_length, 25)
        self.plot_luminosity_per_bunch_before_mask.removeItem(self.ag_curve[2][1])
        self.plot_luminosity_per_bunch_before_mask.removeItem(self.ag_curve[2][0])
        scaling_factor = min(self.model.luminosity_per_bunch_before_mask_data) if abs(
            min(self.model.luminosity_per_bunch_before_mask_data)) > max(
            self.model.luminosity_per_bunch_before_mask_data) else max(
            self.model.luminosity_per_bunch_before_mask_data)

        self.add_curve(3, self.plot_luminosity_per_bunch_before_mask, self.model.luminosity_per_bunch_before_mask_data)
        self.add_curve(4, self.plot_luminosity_per_bunch_before_mask, self.model.mask, pen_arg=mkPen('b'))

        self.ag_curve[2][0] = pg.PlotCurveItem(
            y=ag_curve[0] * scaling_factor, x=ag_range[0],
            fillLevel=1,
            brush=self.ag_brush_0, pen=mkPen(self.ag_brush_0), name='Abort Gap Settings 0')
        self.ag_curve[2][1] = pg.PlotCurveItem(
            y=ag_curve[1] * scaling_factor, x=ag_range[1],
            fillLevel=1,
            brush=self.ag_brush_1, pen=mkPen(None), name='Abort Gap Settings 1')

        self.plot_luminosity_per_bunch_before_mask.addItem(self.ag_curve[2][0])
        self.plot_luminosity_per_bunch_before_mask.addItem(self.ag_curve[2][1])

    def update_per_channel(self):
        ag_curve, ag_range = self.model.compute_curve_and_range(self.model.ag_start, self.model.ag_length, 25)

        self.plot_luminosity_per_channel.removeItem(self.ag_curve[0][0])
        self.plot_luminosity_per_channel.removeItem(self.ag_curve[0][1])
        scaling_factor = min(self.model.luminosity_per_channel_data) if self.model.luminosity_per_channel_data[int(
            ag_range[0][
                0])] < 0 else max(
            self.model.luminosity_per_channel_data)
        self.add_curve(2, self.plot_luminosity_per_channel, self.model.luminosity_per_channel_data)
        self.ag_curve[0][0] = pg.PlotCurveItem(y=ag_curve[0] * scaling_factor,
                                               x=ag_range[0], fillLevel=1,
                                               brush=self.ag_brush_0, pen=mkPen(None), name='Abort Gap Settings 0')

        self.ag_curve[0][1] = pg.PlotCurveItem(y=ag_curve[1] * scaling_factor,
                                               x=ag_range[1], fillLevel=1,
                                               brush=self.ag_brush_1, pen=mkPen(None), name='Abort Gap Settings 1')
        self.plot_luminosity_per_channel.addItem(self.ag_curve[0][0])
        self.plot_luminosity_per_channel.addItem(self.ag_curve[0][1])

    def get_pressed(self):
        if self.model is not None:
            self.model.get_all()

    def subscribe_pressed(self):
        if self.button_subscribe.text() == SUBSCRIBE:
            self.model.create_subscription()
            self.button_subscribe.setText(UNSUBSCRIBE)
        elif self.button_subscribe.text() == UNSUBSCRIBE:
            self.model.delete_all_subscriptions()
            self.button_subscribe.setText(SUBSCRIBE)

    def add_curve(self, curve_index, plot, data, pen_arg=mkPen('w'), x=None, symbol='', symbol_brush=(0, 0, 0, 0)):
        if not self.curve[curve_index]:
            if x is None:
                self.curve[curve_index] = pg.PlotCurveItem(data, pen=pen_arg, symbol=symbol, symbolBrush=symbol_brush)
            else:
                self.curve[curve_index] = pg.PlotDataItem(x=x, y=data
                                                          , pen=pen_arg, symbol=symbol, symbolBrush=symbol_brush)
            plot.addItem(self.curve[curve_index])
        else:
            if x is None:
                x = np.arange(len(data))
            self.curve[curve_index].setData(y=data,
                                            x=x, pen=pen_arg, symbol=symbol, symbolBrush=symbol_brush)
