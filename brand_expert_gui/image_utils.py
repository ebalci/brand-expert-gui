import numpy as np
import pyqtgraph as pg


def generate_image(array: np.array, transpose: bool = False,
                   rgb: bool = True):
    """
    This function takes a 2D numpy array and creates an ImageItem from it.
   :param array: 2D array, composed of 32 bit numbers
    :param color_map_arg: color map to used for the creation of the image
    :param transpose: If true is given, array will be transposed
    :param rgb: If this boolean is False, image will be greyscale
    :return: pyqtgraph ImageItem of given 2D array
    """

    array = array.astype(np.uint32)
    c_map = 'plasma'

    if transpose:
        array = np.ndarray.transpose(array.astype(np.uint32))
    # normalize array
    array = (array - np.min(array)) / np.ptp(array)

    img = pg.ImageItem(array)
    if rgb:
        img.setColorMap('CET-D1A')
    return img
