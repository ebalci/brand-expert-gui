Brand Expert Gui
===================

Introduction
------------

Expert GUI for BRAND FESA Class.

.. warning:: This is just a stub of the documentation for Brand Expert Gui. Please visit
    `BI's documentation on the topic <https://acc-py.web.cern.ch/gitlab/szanzott/pyqt-mega-tutorial-for-be-bi/docs/master/index.html>`_,
    the `official Acc-Py documentation <https://wikis.cern.ch/display/ACCPY/Accelerating+Python+Home>`_ and
    `the Sphinx documentation <https://www.sphinx-doc.org/en/master/index.html>`_
    to learn more about how to write these docpages.

Documentation contents
----------------------

.. toctree::
    :maxdepth: 1
    :hidden:

    self

.. toctree::
    :caption: Brand Expert Gui
    :maxdepth: 1

    usage

.. toctree::
    :caption: Reference docs
    :maxdepth: 1

    api
    genindex

