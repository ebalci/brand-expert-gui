.. _usage:

Usage
=====

A high-level overview of how to use Brand Expert Gui.

An example:

.. testcode::

    import tests
    print(tests.__name__)

.. testoutput::

    tests


Another example:

.. doctest::

    >>> import tests
    >>> tests.__name__
    'tests'
